﻿#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#define M_PI          3.141592653589793238462643383279502884L

using namespace cv;
using namespace std;

Mat matched;
Mat image;
Mat m3;
Mat	gray;
Mat edges;

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
		return -1;
	}

	image = imread("x64\\Debug\\img4.jpg", IMREAD_COLOR); // Read the file

	if (!image.data) // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	m3 = imread("x64\\Debug\\m3.jpg", IMREAD_COLOR); // Read the file

	/// Create the result matrix
	//matched.create(image.rows, image.cols, CV_32FC1);

	matchTemplate(image, m3, matched, CV_TM_CCORR_NORMED);

	double minVal, maxVal;
	Point minLoc, maxLoc;
	minMaxLoc(matched, &minVal, &maxVal, &minLoc, &maxLoc);

	//double x_center = maxLoc.x + m3.cols / 2;
	//double y_center = maxLoc.y + m3.rows / 2;

	// Setup a rectangle to define your region of interest
	cv::Rect myROI(maxLoc.x - m3.cols * 7, maxLoc.y - m3.rows * 2 , m3.cols * 7, m3.rows * 4);

	// Crop the full image to that image contained by the rectangle myROI
	// Note that this doesn't copy the data
	cv::Mat croppedImage = image(myROI);

	// переводим изображение в градации серого

	//cvtColor(image, gray, COLOR_BGR2GRAY);

	// ищем границы алгоритмом Canny
	Canny(croppedImage, edges, 100, 200);
	
	vector<Vec2f> lines;
	HoughLines(edges, lines, 1,  M_PI / 180, 100);

	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(croppedImage, pt1, pt2, Scalar(255, 0, 0), 1, CV_AA);
	}

	//getRotationMatrix2D()

	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Display window", croppedImage); // Show our image inside it.

	waitKey(0); // Wait for a keystroke in the window
	return 0;
}